const { CLAIMS } = require(`@qnzl/auth`)
const authCheck = require(`./_lib/auth`)
const mongoose = require(`mongoose`)
const Account = require('./_Account')
const moment = require(`moment`)
const plaid = require(`plaid`)

const plaidClient = new plaid.Client(
  process.env.PLAID_CLIENT_ID,
  process.env.PLAID_SECRET,
  process.env.PLAID_PUBLIC,
  plaid.environments.development,
  {version: `2018-05-22`}
)

mongoose.connect(process.env.MONGO_URL)

const handler = async (req, res) => {
  const weeksAgo = req.query.weeksAgo || 1

  console.log(`finding available accounts`)
  const accountKeys = await Account.find({})

  if (accountKeys.length === 0) {
    return res.status(400).send('No accounts')
  }

  const transactions = {}
  const accounts = {}

  console.log(`finding transactions from ${weeksAgo} weeks ago`)
  const momentWeeksAgo = moment().subtract(weeksAgo, `week`).format(`YYYY-MM-DD`)

  for (const accountInfo of accountKeys) {
    try {
      const bankInfo = await plaidClient.getTransactions(
        accountInfo.accessToken,
        momentWeeksAgo,
        moment().format(`YYYY-MM-DD`)
      )

      console.log(`adding ${bankInfo.transactions.length} transactions for account ${accountInfo._id}`)

      for(const account of bankInfo.accounts) {
        console.log(`adding ${account.account_id} account for Account ${accountInfo._id}`)

        accounts[account.account_id] = account

        transactions[account.account_id] = bankInfo.transactions.filter((transaction) => {
          return transaction.account_id === account.account_id
        })
      }
    } catch (e) {
      console.log(`error while getting transactions for ${accountInfo._id}:`, e)
    }
  }

  return res.json({
    accounts,
    transactions
  })
}

module.exports = (req, res) => {
  return authCheck(CLAIMS.plaid.dump)(req, res)
}

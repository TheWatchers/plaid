const mongoose = require(`mongoose`)

const AccountSchema = new mongoose.Schema({
  accessToken: String,
  name: String
})

const Account = mongoose.model(`Account`, AccountSchema)

module.exports = Account


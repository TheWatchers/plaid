const mongoose = require(`mongoose`)
const Account = require(`./_Account`)
const plaid = require(`plaid`)

const plaidClient = new plaid.Client(
  process.env.PLAID_CLIENT_ID,
  process.env.PLAID_SECRET,
  process.env.PLAID_PUBLIC,
  plaid.environments.development,
  {version: `2018-05-22`}
)

mongoose.connect(process.env.MONGO_URL)

module.exports = async (req, res) => {
  console.log(`exchanging public token for access token`, req.body)

  const {
    access_token: accessToken
  } = await plaidClient.exchangePublicToken(req.body.token)

  console.log(`created accessToken, storing`)

  await Account.create({ accessToken, name: req.body.name })

  return res.status(201).end()
}
